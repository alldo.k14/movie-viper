//
//  Protocols.swift
//  MovieVIPER
//
//  Created by Alldo Kurniawan on 21/01/23.
//

import Foundation

protocol MovieManagerDelegate {
    func updateGenre(listOfGenre: [ListGenreModel])
    func updateMovie(listOfMovie: [ListMovieModel])
    func updateDetaiMovie(id: Int, name: String, desc: String, duration: Int, releaseDate: String)
    func updateReview(listOfReview: [ReviewModel])
}
