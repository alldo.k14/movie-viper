//
//  ListMovieViewController.swift
//  MovieVIPER
//
//  Created by Alldo Kurniawan on 21/01/23.
//

import UIKit

class ListMovieViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var movieManager = MovieManager()
    var listMovie: [ListMovieModel] = []
    
    var id: Int = 0
    var name: String = ""
    
    var chosenName: String = ""
    var chosenId: Int = 0
    
    var tempPage = 1
    
    var fetchMore = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = name
        
        movieManager.delegate = self
        movieManager.fetchData(code: id, identifier: "chosenMovie", page: tempPage)
        
        self.tableView.register(UINib(nibName: "GenreTableViewCell", bundle: nil), forCellReuseIdentifier: "genreCell")
        tableView.dataSource = self
        tableView.delegate = self
        
    }
    
    convenience init(name: String, id: Int) {
        self.init()
        self.name = name
        self.id = id
    }

}

extension ListMovieViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listMovie.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "genreCell", for: indexPath) as! GenreTableViewCell
        
        cell.titleLabel?.text = listMovie[indexPath.row].movieName
        cell.subTitleLabel?.text = "Rating: \(listMovie[indexPath.row].rating)"
        cell.tag = listMovie[indexPath.row].movieId
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chosenCell = tableView.cellForRow(at: indexPath) as! GenreTableViewCell
        
        chosenName = chosenCell.titleLabel.text ?? ""
        chosenId = tableView.cellForRow(at: indexPath)!.tag
        
        let vc = DetailMovieViewController(name: chosenName, id: chosenId)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !listMovie.isEmpty {
            if indexPath.row == listMovie.count - 1 {
                movieManager.fetchData(code: id, identifier: "chosenMovie", page: tempPage)
            }
        }
    }
    
}

extension ListMovieViewController: MovieManagerDelegate {
    func updateReview(listOfReview: [ReviewModel]) {
    }
    
    func updateDetaiMovie(id: Int, name: String, desc: String, duration: Int, releaseDate: String) {
    }
    
    func updateMovie(listOfMovie: [ListMovieModel]) {
        DispatchQueue.main.async {
            self.listMovie.append(contentsOf: listOfMovie)
            self.tableView.reloadData()
        }
        tempPage += 1
    }
    
    func updateGenre(listOfGenre: [ListGenreModel]) {
    }
    
    
}
