//
//  MainViewController.swift
//  MovieVIPER
//
//  Created by Alldo Kurniawan on 21/01/23.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var movieManager = MovieManager()
    var listGenre: [ListGenreModel] = []
    
    var chosenName: String = ""
    var chosenId: Int = 0
    
    let data = ["One", "Two", "Three"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        movieManager.delegate = self
        movieManager.fetchData(code: 0, identifier: "genre", page: nil)
        
        self.tableView.register(UINib(nibName: "GenreTableViewCell", bundle: nil), forCellReuseIdentifier: "genreCell")
        
        tableView.delegate = self
        tableView.dataSource = self

        // Do any additional setup after loading the view.
    }

}

extension MainViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listGenre.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "genreCell", for: indexPath) as! GenreTableViewCell
        
        cell.subTitleLabel.isHidden = true
        
        cell.titleLabel.text = listGenre[indexPath.row].genreName
        cell.tag = listGenre[indexPath.row].genreId
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chosenCell = tableView.cellForRow(at: indexPath) as! GenreTableViewCell
        
        chosenName = chosenCell.titleLabel.text ?? ""
        chosenId = tableView.cellForRow(at: indexPath)!.tag
        
        let vc = ListMovieViewController(name: chosenName, id: chosenId)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension MainViewController: MovieManagerDelegate {
    func updateReview(listOfReview: [ReviewModel]) {
    }
    
    func updateDetaiMovie(id: Int, name: String, desc: String, duration: Int, releaseDate: String) {
    }
    
    func updateMovie(listOfMovie: [ListMovieModel]) {
    }
    
    func updateGenre(listOfGenre: [ListGenreModel]) {
        DispatchQueue.main.async {
            self.listGenre = listOfGenre
            self.tableView.reloadData()
        }
    }
}
